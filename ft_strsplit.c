/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strsplit.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: vbudnik <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/11/15 21:22:20 by vbudnik           #+#    #+#             */
/*   Updated: 2017/11/17 20:07:42 by vbudnik          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

static int	ft_count_substr(const char *s, char c)
{
	int		count;
	int		substring;

	substring = 0;
	count = 0;
	while (*s != '\0')
	{
		if (substring == 1 && *s == c)
			substring = 0;
		if (substring == 0 && *s != c)
		{
			substring = 1;
			count++;
		}
		s++;
	}
	return (count);
}

static int	ft_lenstr(const char *s, char c)
{
	int	len;

	len = 0;
	while (*s != c && *s != '\0')
	{
		len++;
		s++;
	}
	return (len);
}

char		**ft_strsplit(char const *s, char c)
{
	char	**t;
	int		nb_word;
	int		index;

	if (s == NULL)
		return (NULL);
	index = 0;
	nb_word = ft_count_substr((const char *)s, c);
	t = (char **)malloc(sizeof(*t) * (ft_count_substr((const char *)s, c) + 1));
	if (t == NULL)
		return (NULL);
	while (nb_word--)
	{
		while (*s == c && *s != '\0')
			s++;
		t[index] = ft_strsub((const char *)s, 0, ft_lenstr((const char *)s, c));
		if (t[index] == NULL)
			return (NULL);
		s = s + ft_lenstr(s, c);
		index++;
	}
	t[index] = NULL;
	return (t);
}
